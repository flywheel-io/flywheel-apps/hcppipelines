# Release notes
## 0.0.3_4.3.0
___
- Initial release of the BIDS HCPPipelines gear
### Documentation:
- Add `docs` folder
- Add `docs/release_notes.md`
