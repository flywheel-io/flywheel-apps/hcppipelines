[![CircleCI](https://circleci.com/gh/flywheel-apps/bids-app-template.svg?style=shield)](https://app.circleci.com/pipelines/github/flywheel-apps/bids-app-template)

# BIDS HCPPipelines
A gear for completing HCP preprocessing

The basis of this gear is the [HCPPipelines](https://github.com/BIDS-Apps/HCPPipelines) BIDS app.

## Updates
To update the gear based on newer HCP versions, please submit a pull request
with an amended Dockerfile directly to HCPPipelines. Their updated Docker
image is used as the base for this gear's Dockerfile.

## Quickstart
The gear will use the configuration options to build the command that follows the
HCPPipelines usage:
```
usage: run.py [-h]
              [--participant_label PARTICIPANT_LABEL [PARTICIPANT_LABEL ...]]
              [--session_label SESSION_LABEL [SESSION_LABEL ...]]
              [--n_cpus N_CPUS]
              [--stages {PreFreeSurfer,FreeSurfer,PostFreeSurfer,fMRIVolume,fMRISurface} [{PreFreeSurfer,FreeSurfer,PostFreeSurfer,fMRIVolume,fMRISurface} ...]]
              [--coreg {MSMSulc,FS}] [--gdcoeffs GDCOEFFS] --license_key
              LICENSE_KEY [-v] [--anat_unwarpdir {x,y,z,-x,-y,-z}]
              [--skip_bids_validation]
              bids_dir output_dir {participant}

HCP Pipelines BIDS App (T1w, T2w, fMRI)

positional arguments:
  bids_dir              The directory with the input dataset formatted
                        according to the BIDS standard.
  output_dir            The directory where the output files should be stored.
                        If you are running group level analysis this folder
                        should be prepopulated with the results of
                        theparticipant level analysis.
  {participant}         Level of the analysis that will be performed. Multiple
                        participant level analyses can be run independently
                        (in parallel) using the same output_dir.

optional arguments:
-h, --help            show this help message and exit
--participant_label PARTICIPANT_LABEL [PARTICIPANT_LABEL ...]
						The label of the participant that should be analyzed.
						The label corresponds to sub-<participant_label> from
						the BIDS spec (so it does not include "sub-"). If this
						parameter is not provided all subjects should be
						analyzed. Multiple participants can be specified with
						a space separated list.
--session_label SESSION_LABEL [SESSION_LABEL ...]
						The label of the session that should be analyzed. The
						label corresponds to ses-<session_label> from the BIDS
						spec (so it does not include "ses-"). If this
						parameter is not provided, all sessions should be
						analyzed. Multiple sessions can be specified with a
						space separated list.
--n_cpus N_CPUS       Number of CPUs/cores available to use.
--stages {PreFreeSurfer,FreeSurfer,PostFreeSurfer,fMRIVolume,fMRISurface} [{PreFreeSurfer,FreeSurfer,PostFreeSurfer,fMRIVolume,fMRISurface} ...]
						Which stages to run. Space separated list.
--coreg {MSMSulc,FS}  Coregistration method to use
--gdcoeffs GDCOEFFS   Path to gradients coefficients file
--license_key LICENSE_KEY
						FreeSurfer license key - letters and numbers after "*"
						in the email you received after registration. To
						register (for free) visit
						https://surfer.nmr.mgh.harvard.edu/registration.html
-v, --version         show program's version number and exit
--anat_unwarpdir {x,y,z,x-,y-,z-}
						Unwarp direction for 3D volumes
--skip_bids_validation, --skip-bids-validation
						assume the input dataset is BIDS compliant and skip
						the validation
--processing_mode {hcp,legacy,auto}, --processing-mode {hcp,legacy,auto}
						Control HCP-Pipeline modehcp (HCPStyleData): require
						T2w and fieldmap modalitieslegacy (LegacyStyleData):
						always ignore T2w and fieldmapsauto: use T2w and/or
						fieldmaps if available
--doslicetime         Apply slice timing correction as part of fMRIVolume.
```
## Troubleshooting
- DwellTime is a DICOM field required by HCP for anatomical distortion correction. If the field is not populated
  during the dcm2niix conversion, a DwellTime field can be added to the information section of the
  acquisition. (For testing and testing only, the value can be set to 0.)

## Testing
Since this gear is based on the bids-app-template, testing can be completed within the Docker container.
If the Docker image has already been built, the shell within the container can be accessed through:
```bash
./tests/bin/container-test.sh -B -s
```

The flag "-B" prevents building the docker images, and the "-s" flag drops into
a shell inside the docker container instead of running the tests.
Next, use this command to run the tests inside the container:

```bash
/src/tests/bin/tests.sh
```

This command runs docker and *inside the container* mounts `/src`
at the top level directory of the gear *outside the running container*.
This command runs tests inside the container while, in another
window outside the container, you can edit `run.py`, the modules
in `utils/`, and the tests in `tests/` and then use the above command
to make sure it works.  Running tests and editing code in this way
saves a lot of time because you don't have to re-build the docker
image after every change in the code.

To save even more time, you can run only a specific test by
adding `-- -k <testname>` to the end of the command, for example:

```bash
/src/tests/bin/tests.sh -- -k wet_run_errors
```

### Mimicking a running gear

Testing consists of unit tests and integration tests.  Unit tests test individual
functions in the modules in `utils/`.  The integration tests mimic a gear
running on a Flywheel instance by providing files and directories that are unzipped
inside the running docker container.  Here is what is added to `/flywheel/v0` for
the "dry_run" test:

```bash
    dry_run
    ├── config.json
    ├── input
    │   └── bidsignore
    │       └── bidsignore
    ├── output
    └── work
        └── bids
            ├── dataset_description.json
            └── sub-TOME3024
                └── ses-Session2
                    └── anat
                        ├── sub-TOME3024_ses-Session2_acq-MPR_T1w.json
                        └── sub-TOME3024_ses-Session2_acq-MPR_T1w.nii.gz
```

If you are logged in to a Flywheel instance on your local machine,
the integration tests can make SDK calls on that instance using
your api-key.

In the dry_run test shown above, BIDS formatted data is included
in the test so it does not need to be downloaded.  This gear won't
download data if `work/bids/` exists which saves a lot of time when
developing a BIDS App gear.  The "wet_run" test does download data
following a particular job id found in `config.json` just as the
job running on the platform would.  To make this test work for
you, change the "destination" ID in `config.json` to be the ID of
a valid Analysis container on your Flywheel instance.  Data will
be downloaded depending on the level where that analysis container
is attached (session, subject, or project).

The data for integration tests are stored in zip archives in
`tests/data/gear_tests`.  When creating or editing these integration
tests, the archives need to be unzipped.  When running the tests,
only the zipped files are used.  "Pack" and "unpack" commands are
provided in `tests/bin/` to create the zipped test files and to
allow editing of their contents.  From inside the `gear_tests`
directory, they can be run like this:

```bash
../../bin/pack-gear-tests.py all
../../bin/unpack-gear-tests.py dry_run.zip
```
Using the keyword "all", the first command zips all the *.zip
files in that directory and the second command above unzips only the
"dry_run" test.


# Inputs
### bidsignore (optional)
A list of patterns (like .gitignore syntax) defining files that should be ignored by the
bids validator.

### freesurfer_license (optional)
Your FreeSurfer license file. [Obtaining a license is free](https://surfer.nmr.mgh.harvard.edu/registration.html).
This file will be copied into the $FSHOME directory.  There are [three ways](https://docs.flywheel.io/hc/en-us/articles/360013235453-How-to-include-a-Freesurfer-license-file-in-order-to-run-the-fMRIPrep-gear-)
to provide the license to this gear.  A license is required for this gear to run.

### Run Level
This can run at the
[project](https://docs.flywheel.io/hc/en-us/articles/360017808354-EM-6-1-x-Release-Notes),
[subject](https://docs.flywheel.io/hc/en-us/articles/360038261213-Run-an-analysis-gear-on-a-subject) or
[session](https://docs.flywheel.io/hc/en-us/articles/360015505453-Analysis-Gears) level.
The default is the participant level and is passed on to hcppipelines.

# Configuration Options
Each of the arguments listed above is mirrored in the configuration options.
Note: arguments that start with "gear-" are not passed to the BIDS App.

### n_cpus (optional)
Number of CPUs/cores use.  The default is all available.

### verbose (optional)
Verbosity argument passed to the BIDS App.

### gear-run-bids-validation (optional)
Gear argument: Run bids-validator after downloading BIDS formatted data.  Default is true.

### gear-ignore-bids-errors (optional)
Gear argument: Run BIDS App even if BIDS errors are detected when gear runs bids-validator.

### gear-log-level (optional)
Gear argument: Gear Log verbosity level (INFO|DEBUG)

### gear-save-intermediate-output (optional)
Gear argument: The BIDS App is run in a "work/" directory.  Setting this will save ALL
contents of that directory including downloaded BIDS data.  The file will be named
"\<BIDS App>_work_\<run label>_\<analysis id>.zip"

### gear-intermediate-files (optional)
Gear argument: A space separated list of FILES to retain from the intermediate work
directory.  Files are saved into "\<BIDS App>_work_selected_\<run label>_\<analysis id>.zip"

### gear-intermediate-folders (optional)
Gear argument: A space separated list of FOLDERS to retain from the intermediate work
directory.  Files are saved into "\<BIDS App>_work_selected_\<run label>_\<analysis id>.zip"

### gear-dry-run (optional)
Gear argument: Do everything except actually executing the BIDS App.

### gear-keep-output (optional)
Gear argument: Don't delete output.  Output is always zipped into a single file for
easy download.  Choose this option to prevent output deletion after zipping.

# Workflow
The HCPpipelines gear converts the configuration options into a bash command for the HCPPipelines
BIDS app to process.

# Output
Still TBD.

# Note

This gear was created from the Flywheel [BIDS App Template](https://gitlab.com/flywheel-io/flywheel-apps/bids-app-template) (version
0.0.1).  See the
[bids-app-template/README](https://gitlab.com/flywheel-io/flywheel-apps/bids-app-template/blob/master/README.md)
for information on how to build this gear and run the tests.
